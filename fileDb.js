const fs = require('fs');
const path = './messages/';


module.exports = {
  readItems() {
    const messages = [];
    const files = fs.readdirSync(path);
    files.forEach(file => {
      const fileContent = fs.readFileSync(path + file);
      messages.push(JSON.parse(fileContent.toString()));
    });
    return messages.slice(-5);
  },

  addItem(item) {
    const date = new Date();
    const fileName = date.toISOString();
    fs.writeFileSync(path + fileName, JSON.stringify(item));
    return {...item, datetime: fileName};
  }
}