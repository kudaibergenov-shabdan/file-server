const express = require('express');
const messages = require('./app/messages');

const port = 8000;
const app = express();
app.use(express.json());
app.use('/messages', messages);

app.listen(port, () => {
  console.log(`Server started on ${port} successfully`);
});