const express = require('express');
const router = express.Router();
const fileDb = require('../fileDb');

router.get('/', ((req, res) => {
  const lastFiveMessages = fileDb.readItems();
  res.send(lastFiveMessages);
}));

router.post('/', ((req, res) => {
  if (!req.body.message) {
    return res.status(400).send({error: 'Incoming data not valid'});
  }

  const newMessage = fileDb.addItem({
    message: req.body.message
  });
  res.send(newMessage);
}));

module.exports = router;